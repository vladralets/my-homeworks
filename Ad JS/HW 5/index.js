// Как я понимаю асинхронность? 
// както вот так: https://ibb.co/MG6nhbr

// Асинхронность это когда функция выпадает из потока и выполняется на стороне браузера отдельно, попадая в цыкл и не тормозит выполение других функций, так как JS увы не многопоточная вещь)))))))

const url = 'https://api.ipify.org/?format=json'
btn = document.querySelector('button')



document.querySelector('button').addEventListener('click', async function getUserIp() {
    const response = await fetch(url)
    const ip = await response.json()
    infoCreating(ip.ip)
    console.log(ip.ip);
})

async function infoCreating(info) {
    const response = await fetch(`http://ip-api.com/json/${info}?fields=continent,country,regionName,city,district`)
    const data = await response.json()
    console.log(data);
    fullInfoOut(data)
}



function fullInfoOut(fullInfo) {
    const div = document.createElement('div')
    div.innerHTML = (`<p>Континент: ${fullInfo.continent}</p><p>Страна: ${fullInfo.country}</p><p>Регион: ${fullInfo.regionName}</p><p>Город: ${fullInfo.city}</p><p>Район: ${fullInfo.district}</p>`)
    document.body.append(div)
}
