let link = 'https://ajax.test-danit.com/api/swapi/films'

async function sendRequest(url) {
  let request = await fetch(url)

  let info = await request.json()

  pageCreate(info)
}

function pageCreate(information) {
  information.forEach(info => {
    let {episodeId, name, openingCrawl, characters} = info
    characters.forEach(link => {
      fetch(link)
      .then(response => response.json())
      .then(character => {
        console.log(character)
        let {birthYear, heroname = name, eyeColor} = character
        document.body.insertAdjacentHTML('afterbegin', `<p>Episode: ${episodeId}</p>
        <p>Title: ${name}</p>
        <p>Opening Crawl: ${openingCrawl}</p>
        <ul>
        <li>Name: ${heroname}</li>
        <li>Birth Year: ${birthYear}</li>
        <li>Eye Color: ${eyeColor}</li>
        </ul>`)
      });
      
    });
  });
}

sendRequest(link)


// episode_id, title opening_crawl