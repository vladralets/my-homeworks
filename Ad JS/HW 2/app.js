const books = [
  { 
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70 
  }, 
  {
   author: "Скотт Бэккер",
   name: "Воин-пророк",
  }, 
  { 
    name: "Тысячекратная мысль",
    price: 70
  }, 
  { 
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  }, 
  {
   author: "Дарья Донцова",
   name: "Детектив на диете",
   price: 40
  },
  {
   author: "Дарья Донцова",
   name: "Дед Снегур и Морозочка",
  }
];

let ul = document.createElement('ul')
document.querySelector('#root').insertAdjacentElement('afterbegin', ul)

function showList(list) {
    list.forEach((item, index) => {
        try {
            if (item.author === undefined) {
                throw new Error(`Object ${index + 1}: the author is missing`);
            } 
            else if (item.name === undefined) {
                throw new Error(`Object ${index + 1}: the title is missing`);
            } 
            else if (item.price === undefined) {
                throw new Error(`Object ${index + 1}: the price is missing`);
            } 
            else {
                ul.insertAdjacentHTML("beforeend",
                    `<li>Название: ${item.name}, Автор: ${item.author}, Цена: ${item.price}грн</li>`
                )
            }
        } catch (err) {
            console.log(err)
        }
    })
}

showList(books)


// Приведите пару примеров, когда уместно использовать в коде конструкцию try...catch.

// если есть вероятность, что исполняемый код может выдать ошибку, которая может повлечь за собой остановку выполнения дальнейшего кода