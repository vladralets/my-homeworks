import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Cart from "../components/Cart/Cart";
import Favorites from "../components/Favorites/Favorites";
import Products from "../components/Products/Products";

function AppRoutes({
  cards,
  favorites,
  cart,
  isShowModal,
  addToFav,
  addToCart,
  showModal,
}) {
  return (
    <Switch>
      <Redirect exact from="/" to="/products" />
      <Route exact path="/products">
        <Products
          cards={cards}
          showModal={showModal}
          isShowModal={isShowModal}
          addToFav={addToFav}
          addToCart={addToCart}
        />
      </Route>
      <Route exact path="/cart">
        <Cart
          cards={cards}
          favorites={favorites}
          cart={cart}
          showModal={showModal}
          addToFav={addToFav}
          addToCart={addToCart}
          isShowModal={isShowModal}
        />
      </Route>
      <Route exact path="/favorites">
        <Favorites
          cards={cards}
          favorites={favorites}
          showModal={showModal}
          addToFav={addToFav}
          addToCart={addToCart}
          isShowModal={isShowModal}
        />
      </Route>
    </Switch>
  );
}

export default AppRoutes;
