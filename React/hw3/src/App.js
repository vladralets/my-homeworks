import React, { useState, useEffect } from "react";
import axios from "axios";
import "./App.css";
import Header from "./components/Header/Header";
import AppRoutes from "./routers/AppRoutes";

const App = () => {
  const [cards, setCards] = useState([]);
  const [isShowModal, setShowModal] = useState(false);
  const [targetProd, setTargetProd] = useState(null);
  const [favorites, setFavorites] = useState([]);
  const [cart, setCart] = useState([]);

  if (!localStorage.getItem("cart"))localStorage.setItem("cart", "[]");
  if (!localStorage.getItem("favorites"))localStorage.setItem("favorites", "[]");
  

  useEffect(() => {
    axios("/phones-catalogue.json").then((res) => {
      setCards(res.data);
    });
  }, []);

  const addToFav = (e) => {
    let favProds = JSON.parse(localStorage.getItem("favorites"));
    let newProds = favProds.map((el) => el);
    if (!newProds.includes(e.target.parentElement.id)) {
      newProds.push(e.target.parentElement.id);
      localStorage.setItem("favorites", JSON.stringify(newProds));
      setFavorites(newProds);
      e.target.classList.toggle("active");
    } else if (newProds.includes(e.target.parentElement.id)) {
      let index = newProds.indexOf(e.target.parentElement.id);
      newProds.splice(index, 1);
      localStorage.setItem("favorites", JSON.stringify(newProds));
      setFavorites(newProds);
      e.target.classList.toggle("active");
    }
  };
  const addToCart = () => {
    let cartProds = JSON.parse(localStorage.getItem("cart"));
    let newProds = cartProds.map((el) => el);
    if (!newProds.includes(targetProd)) {
      newProds.push(targetProd);
      localStorage.setItem("cart", JSON.stringify(newProds));
      setCart(newProds);
      setShowModal(false);
    } else if (newProds.includes(targetProd)) {
      let index = newProds.indexOf(targetProd);
      newProds.splice(index, 1);
      localStorage.setItem("cart", JSON.stringify(newProds));
      setCart(newProds);
      setShowModal(false);
    }
  };

  let showModal = (ev) => {
    const check = !isShowModal;
    setShowModal(check);
    setTargetProd(ev.target.id);
  };

  return (
    <div className="App">
      <Header />
      <AppRoutes
        cards={cards}
        cart={cart}
        favorites={favorites}
        isShowModal={isShowModal}
        targetProd={targetProd}
        addToFav={addToFav}
        addToCart={addToCart}
        showModal={showModal}
      />
    </div>
  );
};

export default App;
