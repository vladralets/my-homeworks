import React from "react";
import Card from "../Card/Card";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import "./Favorites.scss";
function Favorites(props) {
  const prodInFav = () => {
    let favProds = JSON.parse(localStorage.getItem("favorites"));
    let prodsArt = props.cards.map((el) => el.article);
    let matched = favProds.filter((el) => prodsArt.indexOf(el) > -1);
    let product = props.cards.map(el=> matched.includes(el.article)?el:null);
    let filteredProduct = product.filter((el) => el);
    return filteredProduct;
  };
  const actions = [
    <Button className="ok-button" key="okbtn" text="Да" func={props.addToCart} />,
    <Button
      className="cancel-button"
      key="cncbtn"
      text="Отмена"
      func={props.showModal}
    />,
  ];

  const card = prodInFav();
  const prodCards = card.map((e) => (
    <Card
      key={e.article}
      card={e}
      fav={props.addToFav}
      favIcon={true}
      func={props.showModal}
      isBtn={true}
    />
  ));

  return (
    <div className="buyer-favorites">
      {props.favorites.length !== 0 ? (
        prodCards
      ) : (
        <p>Вы еще не добавили товары в данную категорию</p>
      )}
      {props.isShowModal ? (
        <Modal
          func={props.showModal}
          header={"Хотите добавить этот товар в корзину?"}
          text={
            <p className="modal-text">
              Вы сможете посмотреть перечень товаров, когда они будут в корзине.
            </p>
          }
          actions={actions}
        />
      ) : null}
    </div>
  );
}

export default Favorites;
