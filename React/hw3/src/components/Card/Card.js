import React from "react";
import "./Card.scss";
import Button from "../Button/Button";
import FavIcon from "../Icon/FavIcon";
import DelIcon from "../Icon/DelIcon";

const Card = (props) => {
    return (

        <div id={props.id} className="product-card">
            {props.delIcon ? (<DelIcon id={props.card.article} className="del-icon" func={props.cartAdd}/>) : null}

            <h3 className='product-name'>{props.card.title}</h3>
            <div className='product-card-container'>
                <div>
                    <img src={props.card.imgUrl} style={{width: 100}} alt={props.card.title}></img>
                </div>
                <div className='product-info'>
                    <p className="product-article">Article: {props.card.article}</p>
                    <p>Цена: {props.card.price} грн</p>
                    <p>Цвет: {props.card.color}</p>
                </div>
            </div>
            <div>
                {props.isBtn
                    ? <Button
                        id={props.card.article}
                        text="Добавить в корзину"
                        className="card-button"
                        func={props.func}
                    /> : null
                }
                {props.favIcon ? (
                    <FavIcon id={props.card.article} className="favor-icon" func={props.fav}/>

                ) : null}

            </div>

        </div>
    );
}


export default Card;
