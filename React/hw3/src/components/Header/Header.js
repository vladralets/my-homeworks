import React from 'react';
import {NavLink} from "react-router-dom";
import './Header.scss'
import logo from '../../Xiaomi_logo.png'

function Header() {
    return (
        <header className='head-bar'>
            <div className='header'>
                <img className="header-logo" src={logo} alt="MERCEDES logo"/>
            </div>
            <NavLink exact to='/products'>Главная</NavLink>
            <NavLink exact to='/favorites'>Избранное</NavLink>
            <NavLink exact to='/cart'>Корзина</NavLink>
        </header>
    );
}

export default Header;