import React from "react";
import Button from "../Button/Button";
import Card from "../Card/Card";
import Modal from "../Modal/Modal";
import "./Products.scss";

const Products = (props)=> {
  const actions = [
    <Button className="ok-button" key="okbtn" text="Ok" func={props.addToCart} />,
    <Button
      className="cancel-button"
      key="cncbtn"
      text="Cancel"
      func={props.showModal}
    />,
  ];

    const prodCards = props.cards.map((e) => (
      <Card id ={e.id}key={e.article} card={e} fav={props.addToFav} func={props.showModal} favIcon={true} isBtn={true}/>
    ));
    return (
    <div className="products-wrapper">{prodCards}
    {props.isShowModal ? (
        <Modal
          func={props.showModal}
          header={"Are you sure you want to add this item to your cart?"}
          text={
            <p className="modal-text">
              The product will be added to your cart, then you can view a list
              of your purchases.
            </p>
          }
          actions={actions}
        />
      ) : null}</div>
    
    )
  }



export default Products;
