import React from "react";
import Button from "../Button/Button";
import Card from "../Card/Card";
import Modal from "../Modal/Modal";
import "./Cart.scss";

function Cart(props) {
  const actions = [
    <Button
      className="ok-button"
      key="okbtn"
      text="Да"
      func={props.addToCart}
    />,
    <Button
      className="cancel-button"
      key="cncbtn"
      text="Отмена"
      func={props.showModal}
    />,
  ];
  const prodInCart = () => {
    let cartProds = JSON.parse(localStorage.getItem("cart"));
    let prodsArt = props.cards.map((el) => el.article);
    let matched = cartProds.filter((el) => prodsArt.indexOf(el) > -1);
    let product = props.cards.map(el =>matched.includes(el.article)?el:null);
    let filteredProduct = product.filter((el) => el);
    return filteredProduct;
  };
  const card = prodInCart();
  const prodCards = card.map((e) => (
    <Card
      id={e.article}
      key={e.article}
      card={e}
      fav={props.addToFav}
      cartAdd={props.showModal}
      favIcon={true}
      delIcon={true}
      isBtn={false}
    />
  ));

  return (
    <div className="buyer-cart">
      {props.cart.length !== 0 ? prodCards : <p>You cart are still empty...</p>}
      {props.isShowModal ? (
        <Modal
          func={props.showModal}
          header={"Товар будет удален с вашей корзины"}
          text={
            <p className="modal-text">
              Удалить товар?
            </p>
          }
          actions={actions}
        />
      ) : null}
    </div>
  );
}

export default Cart;
