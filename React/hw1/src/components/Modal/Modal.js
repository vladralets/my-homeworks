import React, { Component } from "react";
import "./Modal.scss";

class Modal extends Component {
  render() {
    const {header, text, func, backgroundColor, actions, closeButton} = this.props;
    function handleClick(e) {
      if (e.target.className === "modal-wrapper") {
        func();
      }
    }
    return (
      <div className = "modal-wrapper" onClick = {handleClick}>
        <div className = "modal" style = {{ backgroundColor }}>
          <div className = 'modal-header'>
            <h1 className = 'modal-title'>{header}</h1>
            {!!closeButton ? <p className = 'modal-close' onClick = {func} ></p>:null}
          </div>
            {text}
          <div className='modal-btns'>
            {actions}
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
