import React, { Component } from "react";
import "./App.scss";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends Component {
  state = {
    causedModal: null,
  };

  actions = [
    <Button
      className="main-button"
      backgroundColor="rgb(166, 65, 51)"
      key="okbtn"
      text="Ok"
      func={() => alert("Shalom!)")}
    />,
    <Button
      className="main-button"
      backgroundColor="rgb(166, 65, 51)"
      key="cnsbtn"
      func={() =>
        this.setState(
          (state) =>
            (state = {
              ...state,
              causedModal: null,
            })
        )
      }
      text="Cancel"
      color="black"
    />,
  ];

  closeBtn = () => {
    this.setState(
      (state) =>
        (state = {
          ...state,
          causedModal: null,
        })
    );
  };

  handleClick = (e) => {
    return this.setState(
      (state) =>
        (state = {
          ...state,
          causedModal: e.target.id
        })
    );
  };

  render() {
    const { causedModal } = this.state;
    const actions = this.actions;

    return (
      <div className="App">
        <div style={{ margin: 100 + "px" }}>
          <Button
            id="opnfirstmod"
            className="main-button"
            func={this.handleClick}
            backgroundColor="orange"
            text="Open first modal"
          />
          <Button
            id="opnsecmod"
            className="main-button"
            func={this.handleClick}
            backgroundColor="green"
            text="Open second modal"
          />
        </div>
        {causedModal === "opnfirstmod" ? <Modal
                backgroundColor="red"
                closeButton={true}
                func={this.closeBtn}
                header={"Do you want to delete this file?"}
                text={
                  <p className="modal-text">
                    Once you delete this file, it won't be possible to undo this
                    action. Are you sure you want to delete it?
                  </p>
                }
                actions={actions}
              />:null
        }
              {causedModal === 'opnsecmod'?<Modal
                backgroundColor = "green"
                closeButton = {false}
                func = {this.closeBtn}
                header = {"חדשות. הכי קרוב לשמש"}
                text = {
                  <p className="modal-text">
                    אתם בטח שואלים את עצמכם: "מה מַהפְּכָנִי בזה?", אבל אם נגיד לכם שכל ה מָטוֹס יהיה מָסַך מַגָע אחד גדול שמַקרִין לכם את מה שקורה בחוץ?
                    חברה בריטית הציגה פִּיתוּחַ מַהפְּכָנִי בתחום התְעוּפָה – מטוסים ללא חלונות. במטוס ללא החלונות, יוּחלפו דפָנוֹת המטוס במסכים דקים וגמישים, במהלך הטִיסָה יוכלו הנוסעים לצפות במִתרַחֵש מחוץ למטוס, על
                     ידי מַצלֵמָה מיוחדת שתוּצמָד לחלק החיצוני של המטוס ותציג לנוסעים את המִתרַחֵש בחוץ.
                  </p>
                }
                actions = {actions}
              />:null}
      </div>
    );
  }


}

export default App;
