import React from 'react';
import './Header.scss';
import logo from '../../Xiaomi_logo.png';

const Header = () => {
  return (
    <div>
      <div className='header'>
        <img className="header-logo" src={logo} alt="MERCEDES logo" />
      </div>
    </div>
  )
}

export default Header;
