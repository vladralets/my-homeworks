import React, {Component} from "react";
import "./Card.scss";
import Button from "../Button/Button";
import Icon from "../Icon/Icon";
import PropTypes from "prop-types";

class Card extends Component {
    render() {
        const {card, func, icon, fav} = this.props;

        return (
            <div className="product-card">
                <h3 className='product-name'>{card.title}</h3>
                <div className='product-card-container'>
                    <div>
                        <img src={card.imgUrl} style={{width: 150, height: 150}} alt={card.title}/>
                    </div>
                    <div>
                        <p className="product-article">Article: {card.article}</p>
                        <p>Цена: {card.price} UAH</p>
                        <p>Цвет: {card.color}</p>
                    </div>
                </div>
                <div>
                    <Button
                        id={card.article}
                        text="Добавить в корзину"
                        className="card-button"
                        func={func}
                    />
                    {icon ? (
                        <Icon id={card.article} className="favor-icon" func={fav}/>
                    ) : null}
                </div>
            </div>
        );
    }
}

Card.propTypes = {
    card: PropTypes.object.isRequired,
    icon: PropTypes.bool,
    fav: PropTypes.func,
    func: PropTypes.func,
};
Card.defaultProps = {
    card: null,
};

export default Card;
