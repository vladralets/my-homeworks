document.addEventListener('DOMContentLoaded', function () {
    const input = document.createElement('input');
    input.type = 'number'
    input.style.width = '200px';
    input.style.height = '85px';
    input.style.position = 'absolute';
    input.style.top = '325px';
    input.style.left = '635px';
    input.style.outline = 'none'
    input.style.border = "3px solid grey";
    input.style.borderRadius = '8px';
    input.style.fontSize = '40px';

    document.body.append(input);

    const span = document.createElement('span');
    span.innerText = 'Price: ';
    span.style.fontSize = '24pt';
    span.style.position = 'absolute';
    span.style.top = '350px';
    span.style.left = '550px';

    document.body.prepend(span);


    input.onfocus = function () {
        input.style.borderColor = 'green';
        input.style.color = 'green';
    };

    input.onblur = function () {
        input.style.borderColor = 'grey';
        getValue();
    };

    function removeData() {
        const span_value = document.querySelector('.span_value');
        const button_cross = document.querySelector('button');
        span_value.remove();
        button_cross.remove();
        input.value = '';
        input.style.borderColor = 'grey';
        input.style.color = 'grey';
    }

    function getValue() {
        const val = +input.value;

        if (isNaN(val) || val <= 0 || val === '') {
            // input.style.borderColor = 'red';
            getError();
        } else {
            const resultSpan = document.createElement('span');
            resultSpan.className = 'span_value';
            resultSpan.innerHTML = `Поточна ціна: ${val}`;
            resultSpan.style.fontSize = '18pt';
            resultSpan.style.position = 'absolute';
            resultSpan.style.top = '255px';
            resultSpan.style.left = '640px';
            document.body.append(resultSpan);
            input.style.color = 'green';
            buttonFunc();
        }
        
    }

    function buttonFunc() {
        const myButton = document.createElement('button');
        myButton.className = 'button_cross';
        myButton.type = 'submit';
        myButton.style.width = '25px';
        myButton.style.height = '25px';
        myButton.style.border = '1px solid black';
        myButton.style.position ='absolute';
        myButton.style.top = '258px';
        myButton.style.left = '815px';
        myButton.innerHTML = 'X';
        myButton.style.fontSize ='14pt';
        document.body.append(myButton);
        myButton.addEventListener('click', removeData);
    }

    function getError() {
        const textError = document.createElement('span');
        textError.className = 'myError';
        textError.innerHTML = 'Please enter correct price';
        textError.style.color = 'red';
        input.style.color = 'grey';
        input.style.border = "3px solid red";
        textError.style.fontSize = '14pt';
        textError.style.position = 'absolute';
        textError.style.top = '450px';
        textError.style.left = '640px';
        document.body.append(textError);
        input.onfocus = function () {
            input.style.borderColor = 'green';
            input.value = '';
            const myError = document.querySelector('.myError');
            myError.remove();
        }
    }

});



