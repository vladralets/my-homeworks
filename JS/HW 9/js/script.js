
const tabs = document.querySelectorAll('.tabs-title');

for (let itemTabs of tabs) {
    itemTabs.addEventListener('click', function (event) {
        const dataTab = event.target.getAttribute('data-tab');
        const text = document.getElementsByClassName('tabs-content-text');

        for (let i = 0; i < text.length; i++) {
            if (i == dataTab) {
                text[i].style.display = 'block';

            } else {
                text[i].style.display = 'none';
            }
        }
    });
    itemTabs.addEventListener('click',function (event) {
        for (let i = 0; i < tabs.length; i++) {
            tabs[i].className = tabs[i].className.replace(" active", "");
        }
        event.currentTarget.className += " active";
    })
}

