// Функция нужна для сокращения написания кода. Мы можем обернуть код в функция и вызывать его несколько раз, не переписывая его.
// Передавать аргумент нужно, что бы он присвоился параметрам функции.


let num1 = +prompt('First number')
while(isNaN(num1) || num1 == '') {
    num1 = prompt('Please, enter number')
}
let num2 = +prompt('Second number')
while(isNaN(num2) || num2 == '') {
    num2 = prompt('Please, enter number')
}
let sign = prompt('Your sign')
while(sign !== '+' && sign !== '-' && sign !== '/' && sign !== '*') {
    sign = prompt('Please, enter correct sign')
}

function count(num1, num2, sign) {
    switch (sign) {
      case "*":
        return num1 * num2;
      case "+":
        return num1 + num2;
      case "-":
        return num1 - num2;
      case "/":
        if (num2 !== 0) {
          return num1 / num2;
        } else {
          return "не делится на нуль";
        }
    }
  }

  console.log(count(num1, num2, sign));