// DOM - это представление данных обьекта, которые являются структурой и содержимое документа.

function getList(massive, parent = document.body) {
  const newCities = massive.map((item) => {
    return `<li>${item}</li>`
  })
  parent.insertAdjacentHTML('afterbegin', `<ul>${newCities.join(' ')}</ul>`)
}

const place = document.querySelector("#city-list")
getList(["Tel-Aviv", "Netania", "Yaffo", "Erusalem", "Eilat", "Ber-Yakov", 'Ashdot'], place)