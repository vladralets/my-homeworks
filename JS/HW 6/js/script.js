let arr = ['hello', 'world', 23, '23', null];
let newArr = arr.filter(element => typeof element !== "string");
console.log(newArr);

// Метод forEach просто перебирает массив и при этом он ничего не 
// возвращает. Его используют как улучшенный вариант for