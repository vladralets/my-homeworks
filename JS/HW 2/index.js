let userNumber = +prompt('Enter your number', '50')
while (!Number.isInteger(userNumber)) {
    userNumber = +prompt('Enter full number, please.')
}

for (let i = 0; i <= userNumber; i++) {
    if (i % 5 === 0 && i !== 0) {
        console.log(i);
    } 
    if (userNumber < 5) {
        console.log('Sorry, no numbers')
        break
    } 
}

// Циклы нужны для проведения однотипного действия какое то количество раз.
// Могут применяться для вывода интервала цифр или например для вывода товарных позиций из списка на сайте.