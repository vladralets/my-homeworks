
const firstField = document.querySelector('#first_field');
const secondField = document.querySelector('#second_field');

const firstCheck = document.querySelector('.fa-eye');
const secondCheck = document.querySelector('.fa-eye-slash');

secondCheck.classList.replace("fa-eye-slash", "fa-eye");

const buttonCheck = document.querySelector('#btn_check');

const warningMsg = document.createElement('span');
warningMsg.classList.add('warn_msg');
warningMsg.innerHTML = 'You have not entered a value in some field';
warningMsg.style.color = 'red';
warningMsg.style.fontSize = '14pt';
document.body.append(warningMsg);
warningMsg.hidden = true;

const errorMsg = document.createElement('span');
errorMsg.classList.add('err_msg');
errorMsg.innerHTML = 'Нужно ввести одинаковые значения';
errorMsg.style.color = 'red';
errorMsg.style.fontSize = '14pt';
document.body.append(errorMsg);
errorMsg.hidden = true;

firstCheck.addEventListener('click', function () {

    if (firstCheck.classList.contains("fa-eye")) {
        firstField.type = 'text';
        firstCheck.classList.replace("fa-eye", "fa-eye-slash");
    } else if (firstCheck.classList.contains("fa-eye-slash")) {
        firstField.type = 'password';
        firstCheck.classList.replace("fa-eye-slash","fa-eye");
    }

});

secondCheck.addEventListener('click', function () {

    if (secondCheck.classList.contains("fa-eye")) {
        secondField.type = 'text';
        secondCheck.classList.replace("fa-eye", "fa-eye-slash");
    } else if (secondCheck.classList.contains("fa-eye-slash")) {
        secondField.type = 'password';
        secondCheck.classList.replace("fa-eye-slash","fa-eye");
    }

});

function checkValues() {
    let firstValue = firstField.value;
    let secondValue = secondField.value;

    if (firstValue != "" && secondValue != "") {
        if (firstValue == secondValue) {
            warningMsg.hidden = true;
            errorMsg.hidden = true;
            alert('You are welcome');
        } else {
            errorFunc();
        }
    } else {
        emptyFunc();
    }

}

function emptyFunc() {
    warningMsg.hidden = false;
    errorMsg.hidden = true;
}

function errorFunc() {
    errorMsg.hidden = false;
    warningMsg.hidden = true;
}


buttonCheck.addEventListener('click', function (event) {
    event.preventDefault()
    checkValues();
    firstField.value = "";
    secondField.value = "";
});