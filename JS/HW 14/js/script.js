$('.navigation').on('click', () => {
    $('.navigate-menu').slideToggle();
});

$(document).scroll(function () {
    const $screenHeight = $(window).innerHeight();
    const $screenTop = $(window).scrollTop();
    if($screenTop > $screenHeight){
        if(!$('.scrol-top-btn').length) {
            const $scrolTopBtn = $('<div hidden class="scrol-top-btn"></div>');
            $('script:first').before($scrolTopBtn);
            $scrolTopBtn.fadeIn();
            $scrolTopBtn.click(() => {
                $('body, html').animate({
                    scrollTop: 0,
                },);
            });
        }
    } else {
        $('.scrol-top-btn').fadeOut(500, () => {
            $('.scrol-top-btn').remove();
        });
    }
});

const $btnShow = $('<button class="btn-show">Show Hot News</button>');
$('.top-rated').after($btnShow);
$btnShow.click(()=>{
    $('.news').slideToggle();
});
