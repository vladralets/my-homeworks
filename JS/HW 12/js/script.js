document.addEventListener("DOMContentLoaded", function () {
    const stopShow = document.querySelector("#btn_stop")
    const resumeShow = document.querySelector("#btn_resume")
    const images = document.querySelectorAll('.image-to-show')
    let i = 0
    let timerId = window.setInterval(function () {
        images[i].style.display = "none";
        i++
        if (i === images.length) {
            i = 0
        }
        images[i].style.display = "block"
    },3000);
    stopShow.addEventListener('click', function () {
        clearInterval(timerId)
    });
    resumeShow.addEventListener('click', function () {
        timerId = window.setInterval(function () {
            images[i].style.display = "none"
            i++
            if (i === images.length) {
                i = 0
            }
            images[i].style.display = "block"
        },3000)
    })
})


